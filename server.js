'use strict';

// Dependencies
var express = require('express'),
    app     = express(),
    http    = require('http'),
    server  = http.createServer(app),
    env     = process.env.NODE_ENV,
    config  = require('./config.json')[env || 'dev']
;

// Set config
app.set('config', config);

// GET method route
app.get('/', function (req, res) {
    res.send('GET request to the homepage <br/>' + JSON.stringify(req.app.get('config')));
});

// Listen up
server.listen(config.PORT, function () {
    console.log(Date(), 'Server is running in port ' + config.PORT);
});

module.exports = app;
