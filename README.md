### Propósito
Exemplificar a configuração de diferentes ambientes (test|dev|prod) via `config.json`

### Instalar programas necessários
`./install.sh`

### Instalar pacotes
`npm install`

### Rodar a aplicação
É possível rodar a aplicação de 03 modos: `Development|Test|Production`

Por padrão a aplicação roda no modo `development`

As configurações de cada ambiente encontram-se no arquivo `config.json`

// Modo development

`npm start`

// Modo test

`NODE_ENV=test node server`

// Modo production

`NODE_ENV=prod node server`
